# personal dotfiles

os: Arch Custom Runit

Screenshots:
![](https://files.catbox.moe/0mv5nw.png)

dwm:
[Dowload](https://snowfag.0xd.workers.dev/Dotfiles/dwm.tar.zst)

st:
[Dowload](https://snowfag.0xd.workers.dev/Dotfiles/st.tar.zst)

slstatus:
[Dowload](https://snowfag.0xd.workers.dev/Dotfiles/slstatus.tar.zst)

